# MinEd

Text editor with extensive Unicode and CJK support. Convenient and efficient features for editing of plain text documents, programs, HTML etc. User-friendly interface, mouse control and menus.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## MINED.LSM

<table>
<tr><td>title</td><td>MinEd</td></tr>
<tr><td>version</td><td>2022.27</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-12-23</td></tr>
<tr><td>description</td><td>Text editor with Unicode and CJK support</td></tr>
<tr><td>summary</td><td>Text editor with extensive Unicode and CJK support. Convenient and efficient features for editing of plain text documents, programs, HTML etc. User-friendly interface, mouse control and menus.</td></tr>
<tr><td>keywords</td><td>edit, editor, unicode, codepage, encoding</td></tr>
<tr><td>author</td><td>Michiel Huisjes (Minix), Achim M�ller (UNIX), Thomas Wolff &lt;mined -at- towo.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Thomas Wolff &lt;mined -at- towo.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://sourceforge.net/projects/mined/</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://towo.net/mined/</td></tr>
<tr><td>platforms</td><td>DOS, Win32 (Cygwin), *nix</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 3</td></tr>
</table>
