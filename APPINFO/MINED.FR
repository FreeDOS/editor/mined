Begin3
Language:    FR, 850, French (Standard)
Title:       MinEd
Description: �diteur de texte avec support de l'Unicode et du CJK.
Summary:     �diteur de texte avec support �tendu de l'Unicode et du CJK. Fonctions pratiques et efficaces pour l'�dition de documents en texte brut, de programmes, de l'HTML, etc. Interface conviviale, contr�le avec la souris et via des menus.
Keywords:    �diter, �diteur, Unicode, page de code, codage
End
